package com.prueba.servicios.ionix;

import static junit.framework.TestCase.assertTrue;

import com.prueba.servicios.ionix.entity.UserEntity;
import com.prueba.servicios.ionix.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServingWebContentApplicationTest {

	@Autowired
	private UserService userService;

	@Test
	public void crearUsuarioTest(){
		UserEntity usuario = new UserEntity();
		usuario.setEmail("test@test");
		usuario.setFirstname("test1");
		usuario.setLastname("test2");
		usuario.setUsername("test");
		boolean retorno = userService.save(usuario);
		assertTrue(retorno);
	}

}
