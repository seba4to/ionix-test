package com.prueba.servicios.ionix.repositories;


import com.prueba.servicios.ionix.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends
		CrudRepository<UserEntity, Long>,
		JpaRepository<UserEntity, Long> {

}