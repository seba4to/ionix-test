package com.prueba.servicios.ionix.controller;


import com.prueba.servicios.ionix.entity.UserEntity;
import com.prueba.servicios.ionix.services.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(allowedHeaders = "*")

@Log4j2
@RestController
@ResponseBody
@RequestMapping("/usuarios")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    public ResponseEntity<Iterable<UserEntity>> getAllFunction() {
        log.info("In UserController - getAll");
        return ResponseEntity.ok().body(userService.findAll());
    }

    @GetMapping("/findById")
    public ResponseEntity<Optional<UserEntity>> findByIdFunction(@RequestParam Long userId  ) {
        log.info("In UserController - findById");
        return ResponseEntity.ok().body(userService.findById(userId));
    }

    @PostMapping("/getAllPaged")
    public ResponseEntity<Iterable<UserEntity>> getAllPagedFunction(
            @RequestParam int pageNo, @RequestParam int pageSize, @RequestParam(defaultValue = "id") String sortBy) {
        log.info("In UserController - getAllPaged");
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return  ResponseEntity.ok().body(userService.findAll(paging));
    }

    @PostMapping("/save")
    public ResponseEntity<Boolean> saveFunction(@RequestBody UserEntity user) {
        log.info("In UserController - saveFunction");
        return ResponseEntity.ok().body(userService.save(user));
    }

    @PutMapping("/update/{userId}")
    public ResponseEntity<Boolean>  updateFunction(@RequestBody UserEntity user, @PathVariable Long userId) {
        log.info("In UserController - update");
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.update(user));
    }

    @DeleteMapping("/delete/{userId}")
    public ResponseEntity<Boolean>  deleteFunction(@PathVariable Long userId){
        log.info("In UserController - delete");
        userService.deleteById(userId);
        return ResponseEntity.noContent().build();
    }

}
