package com.prueba.servicios.ionix.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

@CrossOrigin(allowedHeaders = "*")
@Log4j2
@RestController
@ResponseBody
@RequestMapping("/data")
public class DataController {

	@GetMapping("/sendData/{text}")
	public String sendData(@RequestParam(defaultValue = "dataEnviada") String text) {
		log.info("In DataController - sendData");
		try {
			String key = "Bar12345Bar12345";
			Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(text.getBytes());
			return "Data a enviar será byte[] = " + new String(encrypted);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
