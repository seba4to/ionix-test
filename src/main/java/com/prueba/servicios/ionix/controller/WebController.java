package com.prueba.servicios.ionix.controller;

import com.prueba.servicios.ionix.entity.UserEntity;
import com.prueba.servicios.ionix.services.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Log4j2
@Controller
public class WebController {

	@Autowired
	private UserController userController;

	@Autowired
	private UserService userService;


	@GetMapping("/listausuarios")
	public String listausuarios(Model model) {
		log.info("In WebController - listausuarios");
		ResponseEntity<Iterable<UserEntity>> data = userController.getAllFunction();
		model.addAttribute("data", userController.getAllFunction().getBody());
		return "listausuarios";
	}

	@GetMapping("/guardarusuario")
	public String guardarusuario(
		@RequestParam(value = "firstname", required = true) String firstname,
		@RequestParam(value = "lastname", required = true) String lastname,
		@RequestParam(value = "email", required = true) String email,
		@RequestParam(value = "username", required = true) String username,
		Model model
	) {
		log.info("In WebController - guardarusuario");
		log.info(firstname + " - " +lastname + " - " +email + " - " +username );

		UserEntity usuario = new UserEntity();

		usuario.setEmail(email);
		usuario.setLastname(lastname);
		usuario.setFirstname(firstname);
		usuario.setUsername(username);

		boolean resultado = userService.save(usuario);

		return "guardarusuarios";
	}

	@GetMapping("/guardarusuarios")
	public String guardarusuarios(
			Model model
	) {
		log.info("In WebController - guardarusuarios sin parámetros");
		return "guardarusuarios";
	}

}
