package com.prueba.servicios.ionix.services.impl;

import com.prueba.servicios.ionix.entity.UserEntity;
import com.prueba.servicios.ionix.repositories.UserRepository;
import com.prueba.servicios.ionix.services.UserService;
//import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

//@Log4j2
@Service
public class UserImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public Iterable<UserEntity> findAll() {
        //log.info("In UserImpl - findAll");
        return userRepository.findAll();
    }

    @Override
    public Iterable<UserEntity> findAll(Pageable paging) {
        //log.info("In UserImpl - findAll paging");
        return userRepository.findAll(paging);
    }

    @Override
    public Optional<UserEntity> findById(long usuarioId) {
        //log.info("In UserImpl - findById");
        return userRepository.findById(usuarioId);
    }

    @Override
    public boolean save(UserEntity usuario) {
        //log.info("In UserImpl - save");
        boolean resultado;
        try {
            userRepository.save(usuario);
            resultado = true;
        }
        catch (Exception e){
            resultado = false;
        }
        return resultado;
    }

    @Override
    public boolean update(UserEntity usuario) {
        //log.info("In UserImpl - update");
        boolean resultado;
        try {
            userRepository.save(usuario);
            resultado = true;
        }
        catch (Exception e){
            resultado = false;
        }
        return resultado;
    }

    @Override
    public boolean deleteById(Long usuarioId) {
        //log.info("In UserImpl - deleteById");
        boolean resultado;
        try {
            userRepository.deleteById(usuarioId);
            resultado = true;
        }
        catch (Exception e){
            resultado = false;
        }
        return resultado;
    }
}
