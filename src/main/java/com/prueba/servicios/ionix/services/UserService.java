package com.prueba.servicios.ionix.services;

import com.prueba.servicios.ionix.entity.UserEntity;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserService {

	public Iterable<UserEntity> findAll();

	public Iterable<UserEntity>  findAll(Pageable paging);

	public Optional<UserEntity> findById(long usuarioId);

	public boolean save(UserEntity usuario);

	public boolean update(UserEntity usuario);

	public boolean deleteById(Long usuarioId);

}
